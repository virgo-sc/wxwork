<?php

namespace Wxwork\Http;

class API
{
    public static $GATEWAY = 'https://qyapi.weixin.qq.com';

    //access_token的获取
    public static $ACCESS_TOKEN = '/cgi-bin/gettoken';
    //三方access_token的获取
    public static $ACCESS_TOKEN_3RD = '/cgi-bin/service/get_corp_token';
    //获取第三方应用凭证
    public static $GET_SUITE_TOKEN = '/cgi-bin/service/get_suite_token';
    //获取企业 jsapi_ticket
    public static $GET_JSAPI_TICKET = '/cgi-bin/get_jsapi_ticket';
    //应用 jsapi_ticket
    public static $GET_AGENT_JSAPI_TICKET = '/cgi-bin/ticket/get';

    public static $GET_PERMANENT_CODE = '/cgi-bin/service/get_permanent_code?suite_access_token=';

    //获取企业授权信息
    public static $GET_AUTH_INFO = '/cgi-bin/service/get_auth_info?suite_access_token=';

    //读取成员
    public static $USER_GET = '/cgi-bin/user/get';

    //获取成员ID列表
    public static $USER_LIST_ID = '/cgi-bin/user/list_id';

    //获取部门列表
    public static $DEPARTMENT_LIST_ID = '/cgi-bin/department/list';

    //获取访问用户身份
    public static $GETUSERINFO= '/cgi-bin/auth/getuserinfo';
    //获取访问用户身份-第三方
    public static $GETUSERINFO3RD = '/cgi-bin/service/auth/getuserinfo3rd';
    //获取客户群详情
    public static $GROUPCHAT = '/cgi-bin/externalcontact/groupchat/get';

    //上传图片
    public static $UPLOADIMG = '/cgi-bin/media/uploadimg';

    //创建企业群发
    public static $MSGTEMPLATE = '/cgi-bin/externalcontact/add_msg_template';

    //获取会话记录
    public static $SYNC_MSG = '/cgi-bin/chatdata/sync_msg';
}
