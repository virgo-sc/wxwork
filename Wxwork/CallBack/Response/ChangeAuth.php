<?php

namespace Wxwork\CallBack\Response;

class ChangeAuth  extends CallBackResponse
{

    public $suiteId;

    public $authCorpId;

    public $type = 'change_auth';

    public function init() {
        $this->suiteId = $this->body['SuiteId'] ?? '';
        $this->authCorpId = $this->body['AuthCorpId'] ?? '';
    }
}
