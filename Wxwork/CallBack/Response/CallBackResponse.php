<?php

namespace Wxwork\CallBack\Response;

class CallBackResponse
{
    public $type = '';

    public $body;

    public function __construct(array $config) {
        $this->type = $config['InfoType'] ?? '';
        $this->body = $config;
        $this->init();
    }

    public function init() {

    }
}
