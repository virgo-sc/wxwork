<?php

namespace Wxwork\CallBack\Response;

class CancelAuth  extends CallBackResponse
{

    public $suiteId;

    public $authCorpId;

    public $type = 'cancel_auth';

    public function init() {
        $this->suiteId = $this->body['SuiteId'] ?? '';
        $this->authCorpId = $this->body['AuthCorpId'] ?? '';
    }
}
