<?php

namespace Wxwork\CallBack\Response;

class EventResponse
{
    public $event = '';

    public $body;

    public function __construct(array $config) {
        $this->event = $config['Event'] ?? '';
        $this->body = $config;
    }
}
