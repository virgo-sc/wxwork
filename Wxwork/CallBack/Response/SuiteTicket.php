<?php

namespace Wxwork\CallBack\Response;

class SuiteTicket  extends CallBackResponse
{

    public $suiteId;

    public $suiteTicket;

    public $type = 'suite_ticket';

    public function init() {
        $this->suiteId = $this->body['SuiteId'] ?? '';
        $this->suiteTicket = $this->body['SuiteTicket'] ?? '';
    }
}
