<?php

namespace Wxwork\CallBack\Response;

class CreateAuth  extends CallBackResponse
{

    public $suiteId;

    public $authCode;

    public $type = 'create_auth';

    public $state;

    public $extraInfo;

    public function init() {
        $this->suiteId = $this->body['SuiteId'] ?? '';
        $this->authCode = $this->body['AuthCode'] ?? '';
        $this->state = $this->body['State'] ?? '';
        $this->extraInfo = $this->body['ExtraInfo'] ?? '';
    }
}
