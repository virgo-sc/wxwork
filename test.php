<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Utils;
use Wxwork\CallBack\CallBack;
use Wxwork\ServiceCorp;

require_once 'vendor/autoload.php';

$config = [

];

require_once 'config.php';

//回调配置
$callBack = new CallBack($config);

$verifyMsg = 'fdcfa514062fd1f9efaa3a71a9284f4947311fdc';
$verifyTimestamp = '1718866445';
$verifyNonce = '1719524280';
$verifyEchoStr = 'ClN4xnTFSnxeM9wX0r3jFsNUb2zUVEnqs5aydSvLJQYDgXnniGwPwNX+TWf7YLMBXSsNAC8JclmfV8Tv6ZR2SiMaE5cRK7zovToXAWz/RY1xI5cj7z+tMWEbgBuStvd9';

$verifyData = '<xml><ToUserName><![CDATA[ww4795871ad3b8429d]]></ToUserName><Encrypt><![CDATA[902HWljbGOJZVM1HnhLnUEMmCFrEqrTC/QS/+oYwgbfBu/iR5jmixBzSJZc8WwnTrKGXFg5QrYXFYeOK0UEvLMGQ7FFFoM/bWybRckVpHFbH0vz0vsGKU/P1l3lLJYWIzCLEO+ROH0US8QIK6oIrkLBWeON3a9ZDqyrHYHNRYtPiTQ87kI3NXLpT7yBSLBlq5jNyU7Tcq/P2V7+mm8+lrBMm4tayIpjvw7hqqKv/Pt3iUqK5Lx2s9Igi8Z2+3VGzfbwUJGAc6ujUBKyHGHZV5C4R2DBudAbaG8k2oFjJHOuVmpS/gkAITX7T7w7XGJtnZA6DG2dK/AwMkJGLGsQhMFZkyUfYvJKggnqkUhO972vhVSUJbbmCNMXpvzvui+uO]]></Encrypt><AgentID><![CDATA[]]></AgentID></xml>';
//验证回调URL
if (0) {
    $result = $callBack->verifyURL($verifyMsg, $verifyTimestamp, $verifyNonce, $verifyEchoStr);
    var_dump($result);
    exit;
}

//指令回调解析
//1-推送suite_ticket
//2-授权通知事件
if (0) {
    //

    $result = $callBack->callbackMsg($verifyMsg, $verifyTimestamp, $verifyNonce, $verifyData);
    var_dump($result);
    exit();
}

//消息回复通知解析

if (0) {
    $result = $callBack->eventMsg($verifyMsg, $verifyTimestamp, $verifyNonce, $verifyData);
    var_dump($result);
    exit();
}


//服务商接口
$serviceCorp = new ServiceCorp($config);

//获取第三方应用凭证
if (0) {
    //$GLOBALS['testResponse'] = '{"suite_access_token":"dLKYJ0Hm6ezkDNte21JugWoseV-14Yt_5ge5jPh3M9_x6D3GlXkCLBAkrww_zHKyyXhBxIbVc2dgFhxa8CV_Inv8XRoeIhpiFh97VdSUiyIog8d--7f9i2ubio9M75iH","expires_in":7200}';

    $suite_ticket = 'PMYX3Cb_iCbJ0DjDsxUUVydXQxMkbmd9QSbfltSdpJUtlqhG0OmtIGxetroTnpN-';
    $suiteAccessToken = $serviceCorp->getSuiteToken($suite_ticket);
    var_dump($suiteAccessToken);
    exit;
}

//获取企业永久授权码
if (0) {
    $suiteAccessToken = 'v86v6GshYjZozAr0OUAYKlkNF_iDy9_HzoqUdEMZgV7RQTC3BBbm1zp18LuYXfcx-iBWKrS6IlGo34L9IRljS-1ZAhTQdfyXtJXkqJalN9vGsGo2ej2nDpf_LWP5BqD2';
    $tempAuthCode = '-fD9D-ud0kBJsjXxhuA1QTUtGdDjH9jR4yDL5XVPt1hmA7wS2ekLhxi-dlXXboQ4BakVgnzSKoMCb_ShODL3ClXU5MR6-gtFvZpKoOzfYYE';
    $result = $serviceCorp->getPermanentCode($suiteAccessToken,$tempAuthCode);
    if (!$result->isSuccess()) {
        var_dump($result->getError());
        return $result->getError();
    } else {
        var_dump($result->getBody());
        return $result->getBody();
    }

    exit();
}

//获取企业授权信息
if (0) {
    $suiteAccessToken = 'dLKYJ0Hm6ezkDNte21JugWoseV-14Yt_5ge5jPh3M9_x6D3GlXkCLBAkrww_zHKyyXhBxIbVc2dgFhxa8CV_Inv8XRoeIhpiFh97VdSUiyIog8d--7f9i2ubio9M75iH';
    $authCorpId = 'wp1KoTbQAAoSzz0WyyJQtSl6WMzPwxpg';
    $permanentCode = 'x5QM44iLC_LpZIUYNnuxeq7YwMO00vm__UcWf8L9VAw';
    $result = $serviceCorp->getAuthInfo($suiteAccessToken,$authCorpId,$permanentCode);
    if (!$result->isSuccess()) {
        var_dump($result->getError());
        return $result->getError();
    } else {
        var_dump($result->getBody());
        return $result->getBody();
    }

    exit();
}

//access_token的获取
if (0) {
    $authCorpId = 'wp1KoTbQAAoSzz0WyyJQtSl6WMzPwxpg';
    $permanentCode = 'Jj-_pcGWXYBl6YU5X4p8rm-Opzss9UITv-hteWEgaB4';
    $result = $serviceCorp->getAccessToken($authCorpId,$permanentCode);

    var_dump($result);
    exit;
}

//读取成员
if (0) {
    $accessToken = 'v6_QbdH4T8oJlSB8R2CUW5aCFjDqiGSvsczGKOq1XBD4ndDCX4_mH0QorZyYWJr3I9AA5VBqk3Ta52jLM612knpB3qIWQ0YE6tT_17Na4wGuX6WaeyjbKY1cX9NdSovjMLupucURr1LVE9LR3NLmzlMUZQGebxaLPTol3xXPiod1Q7_PcmBK45-U8WymmjVmDSsG7S_HkkZyq6iUn547lw';
    $userId = 'wo1KoTbQAA6MrI2L_G-a8VTU7upBK4Sg';
    $result = $serviceCorp->getUser($accessToken,$userId);

    var_dump($result);
    exit;
}

//获取成员ID列表
if (0) {
    $accessToken = 'v6_QbdH4T8oJlSB8R2CUW5aCFjDqiGSvsczGKOq1XBD4ndDCX4_mH0QorZyYWJr3I9AA5VBqk3Ta52jLM612knpB3qIWQ0YE6tT_17Na4wGuX6WaeyjbKY1cX9NdSovjMLupucURr1LVE9LR3NLmzlMUZQGebxaLPTol3xXPiod1Q7_PcmBK45-U8WymmjVmDSsG7S_HkkZyq6iUn547lw';
    $result = $serviceCorp->getUserListId($accessToken);

    var_dump($result);
}

//获取部门列表
if (0) {
    $accessToken = 'v6_QbdH4T8oJlSB8R2CUW5aCFjDqiGSvsczGKOq1XBD4ndDCX4_mH0QorZyYWJr3I9AA5VBqk3Ta52jLM612knpB3qIWQ0YE6tT_17Na4wGuX6WaeyjbKY1cX9NdSovjMLupucURr1LVE9LR3NLmzlMUZQGebxaLPTol3xXPiod1Q7_PcmBK45-U8WymmjVmDSsG7S_HkkZyq6iUn547lw';
    $result = $serviceCorp->getDepartmentListId($accessToken,'');
    var_dump($result);
    exit();
}
